export const data = {
  gardens: [
    {
      id: 1,
      name:'Carols Gardens',
      privacy:'Public',
      creator : 'Caroline', //admin
      members:["Katia"], // can create flowers and petals
      guests:["Sergio"], // can only view 
      flowers:[
        {
          name: 'Discussion about compost',
          petals:[{                                                 // 1st level
            name:'Good Morning',
            creator : 'Caroline Progner',
            atMoment: '0s',
            duration:'1m23s',
            resourceType: 'video',
            resourceUrl: 'https://cloudinary.com/v/123',
            petals:[                                                // 2nd level
              {
                name:'Like',
                creator : 'Katia Levina',
                atMoment: '23s',
                resourceType: 'emoji',
                resourceUrl: 'https://cloudinary.com/v/123',
                petals:[],
              },
              {
                name:'Good Question!',
                creator : 'Katia Levina',
                atMoment: '42s',
                resourceType: 'video',
                resourceUrl: 'https://cloudinary.com/v/123',
                relationLabels:["Reply"],
                petals: [                                         // 3rd level
                  {
                    name:'Like',
                    creator : 'Katia Levina',
                    atMoment: '23s',
                    resourceType: 'emoji',
                    resourceUrl: 'https://cloudinary.com/v/123',
                    petals:[],
                  },  
                ]
              },
            ]
          }]
        }
      ],
    },
    {  
      id: 2,
      name:'Katia Gardens',
      privacy:'Private',
      creator : 'Katia', //admin
      members:["Carol"], // can create flowers and petals
      guests:["Martin"], // can only view 
      flowers:[
        {
          name: 'What is good to plant today!',
          petals:[{                                                 // 1st level
            name:'7 May 2020, flower standup!',
            creator : 'katia',
            atMoment: '0s',
            duration:'1m23s',
            resourceType: 'video',
            resourceUrl: 'https://cloudinary.com/v/123',
            petals:[                                                // 2nd level
              {
                name:'QuestionMark',
                creator : 'Carol',
                atMoment: '23s',
                resourceType: 'emoji',
                resourceUrl: 'https://cloudinary.com/v/123',
                relationLabels:["Please, clarify!"],
                petals:[
                  {
                    name:'',
                    creator : 'Katia Levina',
                    atMoment: '42s',
                    resourceType: 'video',
                    resourceUrl: 'https://cloudinary.com/v/123',
                    relationLabels:["Clarification"],
                    petals: []
                  }
                ],
              },
            ]
          }]
        }
      ],
    },
  ]
};
