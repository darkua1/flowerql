const fetch = require('node-fetch');

describe('call api', ()=> {
  it('should return a sring', async ()=> {
    let name = 'sergio'
    const res = await fetch(`http://localhost:4000/hello/${name}`);
    expect(res.status).toBe(200)
    const body = await res.text()
    expect(typeof body).toBe('string')
  });
  it('should return gardens', async ()=> {
    const query = {query:'{gardens{name}}'};
    const res = await fetch(`http://localhost:4000/`, {
      method:'post',
      body:JSON.stringify(query),
      headers: { 'Content-Type': 'application/json' },
    });
    expect(res.status).toBe(200)
    const {data} = await res.json();
    expect(data.gardens.length).toBe(2)
  });
});