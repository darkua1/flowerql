import graphqlHTTP from 'express-graphql';
import restify from 'restify';
import graphql from 'graphql';
import {data} from './data.js';

console.log('data',data);


//create rest server
const app = restify.createServer();

const {buildSchema} = graphql;
const schema = buildSchema(`
  type Query {
    gardens(name: String): [Garden]
    flowers(garden: String!): [Flower]
  }

  type Mutation {
    updateGardenName(id : Int!, name: String!): Garden
  }

  type Garden {
    name: String
    privacy: String
    creator: String
    members: [String]
    guests: [String]
    flowers: [Flower]
  }

  type Flower {
    name: String
    petals: [Petal]
  }

  type Petal {
    name: String
    creator : String
    atMoment: String
    resourceType: String
    resourceUrl: String
    petals: [Petal]
  }
`);

const getGardenByName = (name)=>{
  return data.gardens.filter((garden)=>{return garden.name.indexOf(name)!=-1})
}

const getGardens = (args)=>{
  if (args.name){
    return getGardenByName((args.name))
  }
  return data.gardens;
}

const getFlowers = (args)=>{
  const garden = getGardenByName(args.name)
  console.log('flowers', garden.flowers)
  return garden.flowers
}

const updateGardenName = (args) => {
  data.gardens.map((garden) => {
    if (garden.id === args.id){
      garden.name = args.name;
      console.log('wtf-inside', garden)
      return garden
    }
  })
  return data.gardens.filter(garden => garden.id === args.id) [0]
}

const resolver = {
  gardens: getGardens,
  flowers: getFlowers,
  updateGardenName: updateGardenName
};

app.get('/',graphqlHTTP({
  schema: schema,
  rootValue: resolver,
  graphiql: true,
}));

app.post('/',graphqlHTTP({
  schema: schema,
  rootValue: resolver,
  graphiql: false,
}))

app.get('/hello/:name',(req,res,next)=>{
  console.log('name',req.params.name);
  res.send(`Hello ${req.params.name}`);
  next()
});

app.listen(process.env.PORT || 4000,'0.0.0.0',()=>{
  console.log(`server started in http://localhost:${process.env.PORT || 4000}`);
});
